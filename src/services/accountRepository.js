import * as config from '../config'

const get = async () => {
  return fetch(config.apiEndpoint + '/b/60507584683e7e079c513dc7', {
    method: 'GET',
    headers: {
      'Content-type': 'application/json; charset=UTF-8',
      'secret-key': config.secretKey
    }
  })
    .then(data => data.json())
}

export default get