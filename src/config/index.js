const apiEndpoint = process.env.REACT_APP_API_ENDPOINT
const secretKey = process.env.REACT_APP_SECRET_KEY || '$2b$10$CqS8fEQkYhSyI4gVJs7JteWrm7dkuMhRbReAlUsaQGwEAarTBI3K.'

export {
  apiEndpoint,
  secretKey
}