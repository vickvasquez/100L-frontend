const options = {
  legend: {
    display: false,
    right: 150
  },
  animation: {
    animateScale: true,
    animateRotate: true
  },
  maintainAspectRatio: false,
  layout: {
    padding: {
      left: -400,
      right: 0,
      top: 0,
      bottom: 0,
    }
  }
}

const summary = {
  colors: [
    '#3ab398',
    '#d9fff7'
  ],
  labels: [
    'Inversión',
    'Plusvalía'
  ],
  titleLegend: 'Inversión + Plusvalía'
}

const industry = {
  colors: [
    '#306aae',
    '#4a90e2',
    '#8bb6ea',
    '#daebff'
  ],
  labels: [
    'Industrial',
    'Comercial',
    'Casa Habitación',
    'Oficina'
  ],
  titleLegend: 'Ladrillos por industria',
}

export {
  summary,
  industry,
  options
}