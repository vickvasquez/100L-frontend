const options = {
  legend: {
    display: false,
  },
  animation: {
    animateScale: true,
    animateRotate: true
  },
  maintainAspectRatio: false,
  layout: {
    padding: {
      left: -190,
      right: 0,
      top: 0,
      bottom: 0,
    }
  }
}

const summary = {
  colors: [
    '#3ab398',
    '#d9fff7'
  ],
  labels: [
    'Renta Actual',
    'Renta Estimada'
  ],
}

export {
  summary,
  options
}