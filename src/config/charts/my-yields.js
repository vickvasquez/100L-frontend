const options = {
  legend: {
    position: 'top',
    align: 'end'
  },
  animation: {
    animateScale: true,
  },
  maintainAspectRatio: false,
  scales: {
    yAxes: [{
      ticks: {
        beginAtZero: true
      }
    }]
  },
  layout: {
    padding: {
      left: 0,
      right: 0,
      top: 0,
      bottom: 0,
    }
  }
}

const colors = {
  salesProfit: {
    backgroundColor: '#9aaac2',
    borderColor: '#9aaac2'
  },
  incomeReceveid: {
    backgroundColor: '#ff5a60',
    borderColor: '#ff5a60'
  },
  others: {
    backgroundColor: '#7ed321',
    borderColor: '#7ed321'
  }
}

const appearence = {
  fill: false,
  borderWidth: 1,
  pointHoverBorderWidth: 20,
  lineTension: 0
}

export {
  options,
  appearence,
  colors
}