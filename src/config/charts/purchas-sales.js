const options = {
  legend: {
    position: 'top',
    align: 'end'
  },
  animation: {
    animateScale: true,
  },
  maintainAspectRatio: false,
  scales: {
    yAxes: [{
      ticks: {
        beginAtZero: true
      }
    }]
  },
  layout: {
    padding: {
      left: 0,
      right: 0,
      top: 0,
      bottom: 30,
    }
  }
}

const colors = {
  earnings: {
    backgroundColor: '#ff6c73',
    borderColor: '#ff6c73'
  },
  lossOfEarnings: {
    backgroundColor: '#4a90e2',
    borderColor: '#4a90e2',
  }
}


const appearence = {
  barPercentage: 0.4,
}

export {
  options,
  appearence,
  colors
}