import { Layout } from './components'
import { MyAccountProvider } from './context/api'

import MyAccount from './pages/MyAccount'
import GlobalStyles from './globalStyles'

function App() {
  return (
    <MyAccountProvider>
      <Layout>
        <GlobalStyles />
        <MyAccount />
      </Layout>

    </MyAccountProvider>
  )
}

export default App;
