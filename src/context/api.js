import React, { createContext, useContext, useState, useEffect } from 'react'

import accountRepository from '../services/accountRepository'

import { Spinner } from '../components'

const AccountContext = createContext()
function MyAccountProvider(props) {
  const [myAccount, setMyAccount] = useState({})
  const [loading, setLoading] = useState(true)
  const [error, setError] = useState(null)

  useEffect(() => {
    setLoading(true)
    setError(null)

    accountRepository()
      .then(data => {
        setMyAccount(data)
      })
      .catch(err => {
        setError(err)
      })
      .finally(() => {
        setLoading(false)
      })
  }, [])

  if (loading) {
    return <Spinner />
  }

  if (error) {
    return <p>Error</p>
  }

  return <AccountContext.Provider
    value={{
      data: myAccount
    }}
    {...props}
  />
}

function useMyAccount() {
  const context = useContext(AccountContext)

  if (context === undefined) {
    throw new Error('Context not initialized')
  }

  return context
}

export { MyAccountProvider, useMyAccount }