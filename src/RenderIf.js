import React from 'react'

const RenderIf = props => {
  const { condition, children } = props

  if (!condition) {
    return null
  }

  return <React.Fragment>{children}</React.Fragment>
}

export default RenderIf