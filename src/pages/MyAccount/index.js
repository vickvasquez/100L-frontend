import React, { useState } from 'react'

import { useMyAccount } from '../../context/api'

import RenderIf from '../../RenderIf'
import { Card, NavSummaryAccount, HeaderPage, NavMoneyYield as NavMyYield, CardSummaryBricks, NavPurchaseSales, PlusvaliaTable } from '../../components'
import { ContainerMyYields, ContainerCard, ContainerChart, ContainerBricks, ContainerPurchaseSales } from './sytles'

import ReportRevenue from './Reports/table-revenue'
import YieldsChart from './Reports/yields'
import SummaryBricks from './Reports/summary-bricks'
import PurchaseSales from './Reports/purchase-sales'
import SummaryRevenue from './Reports/summary-revenue'

const MyAccountPage = () => {
  const [menuActived, setCurrentMenu] = useState(false)
  const { data: myAccount } = useMyAccount()

  const handleClickNavYield = name => {
    setCurrentMenu(name)
  }

  return (
    <React.Fragment>
      <HeaderPage title='Mi cuenta' textButton='Ver flujo de efectivo' />
      <NavSummaryAccount />
      <ContainerCard>
        <HeaderPage title='Mis Rendimientos' variant='h2' />
        <Card>
          <ContainerMyYields>
            <NavMyYield onClick={handleClickNavYield} menuActived={menuActived} />
            <ContainerChart>
              <RenderIf condition={menuActived !== 'capitalGain'}>
                <YieldsChart data={myAccount.data.reports.yields} />
              </RenderIf>
              <RenderIf condition={menuActived === 'capitalGain'}>
                <PlusvaliaTable
                  data={myAccount.data.reports.capitalGain}
                />
              </RenderIf>
            </ContainerChart>
          </ContainerMyYields>
        </Card>
      </ContainerCard>
      <ContainerCard>
        <HeaderPage title='Mis Ladrillos' variant='h2' textButton='Ver mis ladrillos' />
        <Card>
          <ContainerBricks>
            <CardSummaryBricks />
            <SummaryBricks data={myAccount.data.reports.bricks} />
          </ContainerBricks>
        </Card>
      </ContainerCard>
      <ContainerCard>
        <div style={{ display: 'grid', gridTemplateColumns: 'minmax(0, 422px) minmax(0, 1fr)' }}>
          <div style={{ marginRight: '16px' }}>
            <HeaderPage title='Mis Rentas' variant='h2' textButton='Ver mis Rentas' />
            <Card>
              <ContainerPurchaseSales>
                <ReportRevenue />
                <div style={{ marginTop: '32px' }}>
                  <SummaryRevenue data={myAccount.data.reports.revenue} />
                </div>
              </ContainerPurchaseSales>
            </Card>
          </div>
          <div>
            <HeaderPage title='Mis Compras / Ventas' variant='h2' textButton='Histórico de mis Ventas' />
            <Card>
              <ContainerPurchaseSales>
                <NavPurchaseSales data={myAccount.data.reports.summaryBricks} />
                <PurchaseSales data={myAccount.data.reports.purchaseSales} />
              </ContainerPurchaseSales>
            </Card>
          </div>
        </div>
      </ContainerCard>
    </React.Fragment>
  )
}

export default MyAccountPage