import styled from 'styled-components'

export const ContainerMyYields = styled.article`
  display: flex;
  min-height: 459px;
  overflow: auto;
`

export const ContainerChart = styled.div`
  flex: 1;
`

export const ContainerCard = styled.article`
  margin-top: 32px;
`

export const ContainerBricks = styled.div`
  display: grid;
  grid-template-columns: 224px minmax(0, 1fr) minmax(0, 1fr);
  min-height: 214px;
`

export const ContainerPurchaseSales = styled.div`
  box-sizing: border-box;
  padding: 27px 31px;
  height: 429px;
`