import React from 'react'

import * as config from '../../../config/charts/my-yields'

import { Chart } from '../../../components'

const { Line } = Chart

const YieldsChart = props => {
  const configChart = {
    labels: props.data.months,
    datasets: [
      {
        label: 'Utilidad de Ventas',
        data: props.data.utilitySales,
        ...config.colors.salesProfit,
        ...config.appearence
      },
      {
        label: 'Rentas Recibidas',
        data: props.data.revenueReceived,
        ...config.colors.incomeReceveid,
        ...config.appearence
      },
      {
        label: 'Otros',
        data: props.data.others,
        ...config.colors.others,
        ...config.appearence
      }
    ]
  }

  return <Line data={configChart} options={config.options} />
}

export default YieldsChart