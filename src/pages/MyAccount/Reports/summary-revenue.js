import React from 'react'

import * as config from '../../../config/charts/rentas'

import { Chart } from '../../../components'
const { Doughnut } = Chart

const SummaryRevenue = props => {
  const { data } = props

  const configRevenueChart = {
    labels: config.summary.labels,
    datasets: [{
      data: [
        data.current,
        data.estimate
      ],
      backgroundColor: config.summary.colors
    }]
  }
  return (
    <Doughnut
      data={configRevenueChart}
      options={config.options}
    />
  )
}

export default SummaryRevenue