import React from 'react'

import * as config from '../../../config/charts/bricks-summary'

import { Chart } from '../../../components'
const { Doughnut } = Chart

const SummaryBricks = props => {
  const { data } = props

  const configSummaryBricks = {
    labels: config.summary.labels,
    titleLegend: config.summary.titleLegend,
    datasets: [{
      data: [
        data.investment,
        data.capitalGain
      ],
      backgroundColor: config.summary.colors
    }]
  }

  const configByIndustry = {
    labels: config.industry.labels,
    titleLegend: config.industry.titleLegend,
    datasets: [{
      data: [
        data.industrial,
        data.comercial,
        data.home,
        data.office
      ],
      backgroundColor: config.industry.colors
    }]
  }

  return <>
    <div>
      <Doughnut
        data={configSummaryBricks}
        options={config.options}
        hasTotal
      />
    </div>
    <div>
      <Doughnut
        data={configByIndustry}
        options={config.options}
        hasTotal
      />
    </div>
  </>
}

export default SummaryBricks