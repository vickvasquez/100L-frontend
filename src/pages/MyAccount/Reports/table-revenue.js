import React from 'react'

import { Table, Icon } from '../../../components'

const RevenueReport = () => {
  const data = [
    {
      concepto: 'Renta Actual',
      anual: 2323,
      mensual: 232
    },
    {
      concepto: 'Renta Estimada Área Disponible',
      anual: 23234,
      mensual: 2020
    },
    {
      concepto: 'Renta Anual Actual + Estimada ',
      anual: 23234,
      mensual: 2020
    }
  ]
  const columns = [
    {
      label: 'Concepto',
      render: (row) => {
        return (
          <div style={{ display: 'flex', alignItems: 'center' }}>
            <Icon type='rentaAnualEstimada1' style={{ marginRight: '15px' }} />
            <span>{row.concepto}</span>
          </div>
        )
      }
    },
    {
      label: 'Mensual',
      prop: 'mensual',
      render: row => {
        return (
          <span>$ {row.mensual}</span>
        )
      }
    },
    {
      label: 'Anual',
      prop: 'anual',
      render: row => {
        return (
          <span>$ {row.anual}</span>
        )
      }
    }
  ]


  return (
    <Table
      data={data}
      columns={columns}
    />
  )
}

export default RevenueReport