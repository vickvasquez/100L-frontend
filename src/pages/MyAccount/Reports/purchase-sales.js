import React from 'react'

import * as config from '../../../config/charts/purchas-sales'

import { Chart } from '../../../components'

const { Bar } = Chart

const PurchaseSales = props => {
const { data } = props

  const configChart = {
    labels: data.months,
    datasets: [
      {
        label: data.earnings.label,
        borderColor: config.colors.earnings.borderColor,
        backgroundColor: config.colors.earnings.backgroundColor,
        data: data.earnings.data,
        ...config.appearence
      },
      {
        label: data.lossOfEarnings.label,
        borderColor: config.colors.lossOfEarnings.borderColor,
        backgroundColor: config.colors.lossOfEarnings.backgroundColor,
        data: data.lossOfEarnings.data,
        ...config.appearence
      }
    ]
  }
  return (
    <Bar data={configChart} options={config.options} />
  )
}

export default PurchaseSales