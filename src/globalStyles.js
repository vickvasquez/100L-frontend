import { createGlobalStyle } from 'styled-components'
import { normalize } from 'styled-normalize'

const GlobalStyles = createGlobalStyle`
  ${normalize}

  body {
    margin: 0;
    padding: 0;
    background-color: #f8f8f8;
    height: 100%;
    font-family: 'Roboto', Arial, Helvetica, sans-serif;
    color: #3e3e3e;
  }

  #root {
    position: fixed;
    left: 0;
    bottom: 0;
    right: 0;
    top: 0;
    overflow-x: auto
  }

  p, ul {
    margin: 0;
    padding: 0;
  }
`

export default GlobalStyles;