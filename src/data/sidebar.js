const items = [
  {
    label: 'Mi cuenta',
    icon: 'miCuenta'
  },
  {
    label: 'Comprar',
    icon: 'comprar'
  },
  {
    label: 'Vender',
    icon: 'vender'
  },
  {
    label: 'Agregar fondos',
    icon: 'agregarFondos'
  },
  {
    label: 'Retirar fondos',
    icon: 'retirarFondos'
  },
  {
    label: 'Movimientos',
    icon: 'movimientosActiva'
  },
  {
    icon: 'ayuda'
  }
]

export default items