import React from 'react'
import RenderIf from '../../RenderIf'

import { RowWrapper, CellWrapper, TableWrapper, CaptionWrapper, Angle } from './styles'
const Caption = ({ columns }) => {
  return (
    <thead>
      <CaptionWrapper>
        {columns.map(column => {
          return <th key={column.label}>
            {column.label}
            <RenderIf condition={column.sort}>
              <Angle aria-hidden='true' />
            </RenderIf>
          </th>
        })}
      </CaptionWrapper>
    </thead>
  )
}

const Cell = ({ row, column }) => {
  const isFunction = (column.render && typeof column.render === 'function')
  let CellFormatted = ''

  if (isFunction) {
    CellFormatted = column.render(row)
  }

  return (
    <CellWrapper>
      <RenderIf condition={(isFunction && typeof CellFormatted === 'object')}>
        {CellFormatted}
      </RenderIf>

      <RenderIf condition={!isFunction}>
        {row[column.prop]}
      </RenderIf>
    </CellWrapper>
  )
}

const Row = ({ row, columns }) => {
  return (
    <RowWrapper>
      {columns.map(column => <Cell row={row} column={column} key={column.label} />)}
    </RowWrapper>
  )
}
const Body = ({ data = [], columns = [] }) => {
  return (
    <tbody>
      {data.map((row, i) => <Row row={row} columns={columns} key={i} />)}
    </tbody>
  )
}

const Table = props => {
  const { data, columns } = props

  return (
    <TableWrapper>
      <Caption columns={columns} />
      <Body data={data} columns={columns} />
    </TableWrapper>
  )
}

export default Table