import styled from 'styled-components'

export const TableWrapper = styled.table`
  width: 100%;
  border-collapse: collapse;
`
export const RowWrapper = styled.tr`
  cursor: pointer;

  transition: all ease-in .2s;

  &:hover {
    background-color: #feabae;
  }

  &:hover > td {
    font-weight: 700;
  }

  &:nth-child(even) {
    background-color: #f1f1f1;
  }
`
export const CellWrapper = styled.td`
  border-bottom: 1px solid #e3e3e3;
  padding: 8px 13px;
  font-size: 14px;
  font-weight: 300;
  max-width: 135px;
`

export const CaptionWrapper = styled.tr`
  background-color: #9c9c9c;

  th {
    font-size: 12px;
    color: #fff;
    text-align: left;
    padding: 11px 13px;
    position: relative;
  }
`

export const Angle = styled.span`
  position: absolute;
  top: 47%;
  width: 0;
  height: 0;
  left: 65%;
  border-left: 5px solid transparent;
  border-right: 5px solid transparent;
  right: -1.4rem;
  border-top: 5px solid #fff;
  transition: transform 0.35s cubic-bezier(0.65, 0.05, 0.36, 1),top ease-in 0.2s;
  cursor: pointer;

  [aria-hidden='false'] {
    transform: rotate(180deg)
  }
`