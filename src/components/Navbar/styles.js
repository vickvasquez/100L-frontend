import styled from 'styled-components'

import { SidebarItem, ContainerItems } from '../Sidebar/styles'

export const Navbar = styled.div`
  background-color: rgba(255, 255, 255, 0.55);
  display: flex;
  justify-content: flex-end;
  width: 100%;
  padding: 20px 32px;
  box-sizing: border-box;
  border-bottom: solid 1px #e1e1e1;
`

export const Menu = styled(ContainerItems)`
  flex-direction: row;
`

export const MenuItem = styled(SidebarItem)`
  justify-content: center;
  flex-direction: row;
  padding: 0;
  margin-right: 32px;

  &:first-child {
    margin-right: 35px;
  }

  &:last-child {
    margin-right: 0;
  }
`

export const Avatar = styled.img`
  border-radius: 50%;
  height: 34px;
  width: 35px;
`

export const Username = styled.span`
  font-size: 14px;
  font-stretch: condensed;
  margin-left: 8px;
`