import React from 'react'

import { Icon } from '..'
import { Navbar, Menu, MenuItem, Avatar, Username } from './styles'
import { Link } from '../Sidebar/styles'

const SIZE_ICONS = 25
const NavbarComponent = props => {
  const { username = '', avatar = '' } = props

  return (
    <Navbar>
      <Menu>
        <MenuItem>
          <Avatar src={avatar} alt={username} />
          <Username>{username}</Username>
        </MenuItem>
        <MenuItem>
          <Link href='/'>
            <Icon type='configuracion' width={SIZE_ICONS} height={SIZE_ICONS} />
          </Link>
        </MenuItem>
        <MenuItem>
          <Link href='/'>
            <Icon type='notificacionActiva' width={SIZE_ICONS} height={SIZE_ICONS} />
          </Link>
        </MenuItem>
        <MenuItem>
          <Link href='/'>
            <Icon type='logout' width={SIZE_ICONS} height={SIZE_ICONS} />
          </Link>
        </MenuItem>
      </Menu>
    </Navbar>
  )
}

export default NavbarComponent