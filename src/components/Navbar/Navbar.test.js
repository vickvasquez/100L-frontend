import { render, screen } from '@testing-library/react'

import Navbar from '.'

describe('Navbar component', () => {
  it('should display navbar', () => {
    render(<Navbar />)

    const navbar = screen.getByRole('list')
    expect(navbar).toBeInTheDocument()
  })

  describe('should have', () => {
    it('avatar', () => {
      render(<Navbar avatar='/img/avatar.jpg' username='user'/>)

      const avatar = screen.getByAltText('user')
      expect(avatar.src).toContain('/img/avatar.jpg')
    })

    it('username', () => {
      render(<Navbar username='user' />)

      const username = screen.getByText('user')
      expect(username).toBeInTheDocument()
    })
  })
})