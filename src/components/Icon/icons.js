import comprar from './assets/comprar.svg'
import handshake from './assets/handshake.svg'
import logout from './assets/log-out.svg'
import logotipo from './assets/logotipo.svg'
import miCuenta from './assets/micuenta-on.svg'
import movimientosActiva from './assets/movimietos-active.svg'
import vender from './assets/nav-bar-vender.svg'
import notificacionActiva from './assets/notification-active.svg'
import rentaAnualActual from './assets/renta-anual-actual.svg'
import rentaAnualActual1 from './assets/renta-anual-actual-1.svg'
import rentaAnualEstimada from './assets/renta-anual-estimada.svg'
import rentaAnualEstimada1 from './assets/renta-anual-estimada-1.svg'
import retirarFondos from './assets/retirar-fondos.svg'
import agregarFondos from './assets/agregar-fondos.svg'
import configuracion from './assets/settings.svg'
import ayuda from './assets/ayuda.svg'
import plus from './assets/plus.png'
import ladrillosDisponibles from './assets/ladrillos-disponible.svg'
import grupos from './assets/group-9.svg'
import less from './assets/1225028-200@2x.png'

const icons = {
  comprar,
  handshake,
  logout,
  logotipo,
  miCuenta,
  movimientosActiva,
  vender,
  notificacionActiva,
  rentaAnualActual,
  rentaAnualActual1,
  rentaAnualEstimada,
  retirarFondos,
  configuracion,
  ayuda,
  agregarFondos,
  plus,
  ladrillosDisponibles,
  grupos,
  less,
  rentaAnualEstimada1
}

export default icons