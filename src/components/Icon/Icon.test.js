import { render, screen } from '@testing-library/react'
import { axe } from 'jest-axe'

import Icon from '.'

describe('Icon component', () => {
  it('should be accessible', async () => {
    const { container } = render(<Icon type='comprar' />)

    const result = await axe(container)
    expect(result).toHaveNoViolations()
  })


  it('should display a icon', () => {
    render(<Icon type='comprar' />)

    const icon = screen.getByRole('img')
    expect(icon).toBeInTheDocument()
  })
})