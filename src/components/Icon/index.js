import React from 'react'
import icons from './icons'
import PropTypes from 'prop-types'

const Icon = props => {
  const { type, ...rest } = props

  const icon = icons[type]

  return (
    <img src={icon} alt={icon} {...rest} />
  )
}

Icon.propTypes = {
  type: PropTypes.oneOf(Object.keys(icons)),
}

Icon.defaultProps = {
  height: 32,
  width: 32
}

export default Icon