import styled from 'styled-components'

export const PropertyWrapper = styled.a`
    color: #5c8f84;
    text-transform: capitalize;
    display: block;
    padding: 8px 0
`