import React from 'react'
import { Table } from '..'

import { PropertyWrapper } from './styles'

const PlusvaliaTable = props => {
  const columns = [
    {
      label: 'Propiedad',
      prop: 'property',
      render: row => {
        return (
          <PropertyWrapper href={`/detail/$${row.uuid}`}>${row.property}</PropertyWrapper>
        )
      }
    },
    {
      label: 'Código de Ladrillo',
      prop: 'code',
      sort: true,
      render: row => {
        return (
          <span>${row.code}</span>
        )
      }
    },
    {
      label: 'Tipo de propiedad',
      prop: 'type',
      sort: true,
      render: row => {
        return (
          <span>${row.type}</span>
        )
      }
    },
    {
      label: 'Precio compra',
      prop: 'purchasePrice',
      sort: true,
      render: row => {
        return (
          <span>${row.purchasePrice}</span>
        )
      }
    },
    {
      label: 'Precio última Transacción',
      prop: 'lastTransactionPrice',
      sort: true,
      render: row => {
        return (
          <span>${row.lastTransactionPrice}</span>
        )
      }
    },
    {
      label: 'Plusvalía',
      prop: 'plusvalia',
      sort: true,
      render: row => {
        return (
          <span>${row.plusvalia}</span>
        )
      }
    }
  ]

  const { data } = props

  return (
    <Table
      data={data}
      columns={columns}
    />
  )
}

export default PlusvaliaTable