import React from 'react'
import { Button } from '..'
import { TitlePage, Header, SubtitlePage } from './styles'

import RenderIf from '../../RenderIf'

const HeaderPageComponent = props => {
  const { title, subtitle = title,  onClick = () => {}, textButton, variant, className = '' } = props

  const isTitle = !variant || variant === 'h1'
  return (
    <Header className={className}>
      <RenderIf condition={isTitle}>
        <TitlePage>{title}</TitlePage>
      </RenderIf>
      <RenderIf condition={variant=== 'h2'}>
        <SubtitlePage>{subtitle}</SubtitlePage>
      </RenderIf>
      <RenderIf condition={textButton}>
        <Button onClick={onClick}>{textButton}</Button>
      </RenderIf>
    </Header>
  )
}

export default HeaderPageComponent