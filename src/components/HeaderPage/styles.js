import styled, { css } from 'styled-components'

export const Header = styled.header`
  display: flex;
  align-items: center;
  justify-content: space-between;
  margin-bottom: 25px;
`

const title = css`
  font-size: 24px;
  margin: 0;
  padding: 0;
`

export const TitlePage = styled.h1`
  font-weight: 700;
  ${title}
`

export const SubtitlePage = styled.h2`
  font-weight: 300;
  ${title}
`