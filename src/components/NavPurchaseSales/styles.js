import styled from 'styled-components'

export const NavPurchaseSales = styled.div`
  display: flex;
  align-items: center;
`

export const MenuPurchaseSales = styled.ul`
  border-radius: 8px;
  border: solid 1px #eeecec;
  background-color: #fafafa;
  display: flex;
  justify-content: space-between;
  flex: 1;
  margin-left: 35px;

  li {
    display: flex;
    align-items: center;
    margin: 0%;
    padding: 9px 20px;
    list-style: none;
    text-align: center;
    position: relative;

    strong {
      font-size: 34px;
    }

    span {
      font-size: 14px;
      margin-left: 5px;
    }

    img {
      position: absolute;
      right: -25px;
    }

    &:first-child {
      border-right: 1px solid #eeecec;
      border-radius: 8px 0 0 8px;
      background-color: #fff;
    }

    &:last-child {
      border-right: none;

      img {
        position: absolute;
        left: -25px;
      }

      strong {
        color: red;
      }
    }
  }
`