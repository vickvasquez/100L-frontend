import React from 'react'

import { Icon } from '..'

import { NavPurchaseSales, MenuPurchaseSales } from './styles'

const NavPurchaseSalesComponent = props => {
  const { data = {} } = props

  return (
    <NavPurchaseSales>
      <Icon type='grupos' width={59} height={54} />
      <MenuPurchaseSales>
        <li>
          <strong>{data.totalBricks}</strong>
          <span>Ladrillos totales</span>
        </li>
        <li>
          <strong>{data.genesisBricks}</strong>
          <span>Ladrillos Génesis</span>
          <Icon type='plus' width={25} height={25} />
        </li>
        <li>
          <strong>{data.purchasedBricks}</strong>
          <span>Ladrillos Comprados</span>
        </li>
        <li>
          <Icon type='less' width={25} height={25} />
          <strong>{data.soldBricks}</strong>
          <span>Ladrillos Vendidos</span>
        </li>
      </MenuPurchaseSales>
    </NavPurchaseSales>
  )
}

export default NavPurchaseSalesComponent