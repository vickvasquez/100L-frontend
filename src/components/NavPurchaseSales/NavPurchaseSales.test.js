import { render, screen } from '@testing-library/react'
import { axe } from 'jest-axe'

import NavPurchaseSales from '.'

const summary = {
  'totalBricks': 100,
  'genesisBricks': 102,
  'soldBricks': 193,
  'purchasedBricks': 23
}

const items = Object.keys(summary)

describe('NavPurchaseSales component', () => {
  it('should be accesible', async () => {
    const { container } = render(<NavPurchaseSales data={summary} />)

    const result = await axe(container)
    expect(result).toHaveNoViolations()
  })

  test.each(items)('should be have %s', value => {
    render(<NavPurchaseSales data={summary} />)

    const item = screen.getByText(new RegExp(summary[value], 'i'))

    expect(item).toBeInTheDocument()
  })
})