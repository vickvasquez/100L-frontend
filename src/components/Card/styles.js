import styled from 'styled-components'

export const Card = styled.div`
  background-color: #fefefe;
  border-radius: 8px;
  border: solid 1px #e3e3e3;
`