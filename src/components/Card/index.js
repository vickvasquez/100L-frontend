import React from 'react'

import { Card } from './styles'

const CardComponent = props => {
  const { children } = props
  return <Card>{children}</Card>
}

export default CardComponent