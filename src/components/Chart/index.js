import Line from './lines'
import Doughnut from './doughnut'
import Bar from './bar'

let Charta = {}

Charta = {
  Line,
  Doughnut,
  Bar
}

export default Charta