import React from 'react'
import { Bar } from 'react-chartjs-2'

const CharBarComponent = props => {
  const { data, options } = props
  return (
    <Bar
      data={data}
      options={options}
      type='line'
    />
  )
}

export default CharBarComponent