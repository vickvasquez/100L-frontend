import styled from 'styled-components'

export const ContainerDoughnut = styled.div`
  display: flex;
  align-items: center;
  position: relative;
  height: 100%;
`

export const ContainerTable = styled.div`
  position: absolute;
  right: 0px;
`

export const TitleLegend = styled.h3`
  font-weight: 300;
  font-size: 20px;
  margin: 0;
  padding: 0;
  padding-top: 12px;
  margin-bottom: 13px;
`

export const ItemLabel = styled.p`
  font-size: 14px;
  padding: 5px 0;
`

export const ItemLabelBold = styled(ItemLabel)`
  font-weight: 700;
  color: #696969;
`

export const BadgeLegend = styled.span`
  display: inline-block;
  height: 14px;
  width: 17px;
  margin-right: 5px;
  vertical-align: middle;
`

export const Table = styled.table`
  td {
    padding: 0 10px;

    &:first-child {
      padding-left: 0;
    }
  }
`
export const Divider = styled.hr`
  background-color: #e3e3e3;
  border: 0;
  height: 1px;
  width: 100%;
`