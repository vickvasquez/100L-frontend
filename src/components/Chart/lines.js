import React from 'react'
import { Line, defaults } from 'react-chartjs-2'

defaults.global.legend.labels.usePointStyle = true
defaults.global.legend.labels.fontColor = '#7d829a'

const LineChart = props => {
  return (
    <Line
      data={props.data}
      options={props.options}
    />
  )
}

export default LineChart