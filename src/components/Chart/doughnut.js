import React from 'react'
import { Doughnut } from 'react-chartjs-2'
import RenderIf from '../../RenderIf'

import { ContainerDoughnut, ContainerTable, BadgeLegend, ItemLabelBold, ItemLabel, Divider, Table, TitleLegend } from './styles'

const RenderCells = props => {
  const { label, index, data, hasTotal } = props
  return (
    <tr>
      <td>
        <BadgeLegend style={{ backgroundColor: data.datasets[0].backgroundColor[index] }} />
        <ItemLabel style={{ display: 'inline-block' }}>{label}</ItemLabel>
      </td>
      <td><ItemLabelBold>{data.datasets[0].data[index]}%</ItemLabelBold></td>
      <RenderIf condition={hasTotal}>
        <td><ItemLabel>$150,000</ItemLabel></td>
      </RenderIf>
    </tr>
  )
}

const RenderDataChart = props => {
  const { data, hasTotal } = props
  return (
    <>
      <Table>
        <tbody>
          {data.labels.map((label, i) => <RenderCells data={data} label={label} key={i} index={i} hasTotal={hasTotal} />)}
          <RenderIf condition={hasTotal}>
            <tr>
              <td colSpan={4}>
                <Divider />
              </td>
            </tr>
            <tr>
              <td colSpan={2} align='right'><ItemLabel>Total</ItemLabel></td>
              <td><ItemLabelBold>$700,000</ItemLabelBold></td>
            </tr>
          </RenderIf>
        </tbody>
      </Table>
    </>
  )
}

const RenderLegend = props => {
  const { data, hasTotal, options } = props
  return (
    <ContainerTable style={{ right: options.legend.right }}>
      <RenderIf condition={data.titleLegend && data.titleLegend !== ''}>
        <TitleLegend>
          {data.titleLegend}
        </TitleLegend>
      </RenderIf>
      <RenderDataChart data={data} hasTotal={hasTotal} />
    </ContainerTable>

  )
}
const DoughnutComponent = props => {
  const { data, options, hasTotal = false } = props

  return (
    <ContainerDoughnut>
      <Doughnut
        data={data}
        options={options}
      />
      <RenderLegend data={data} hasTotal={hasTotal} options={options} />
    </ContainerDoughnut>
  )
}

export default DoughnutComponent