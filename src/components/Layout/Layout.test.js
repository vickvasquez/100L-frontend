import { render, screen } from '@testing-library/react'
import Layout from '.';

describe('Layout component', () => {
  it('should display layout component', () => {
    render(<Layout />)
    const layout = screen.getByRole('main')
    expect(layout).toBeInTheDocument()
  })
})