import React from 'react'

import { Layout, Content } from './styles'

import { Sidebar, Navbar } from '..'

import avatar from '../../assets/img/profile-pic.png'

const LayoutComponent = ({ children }) => {
  return (
    <Layout role='main'>
      <Sidebar />
      <section>
        <Navbar username='Mariana Garcia' avatar={avatar} />
        <Content>
          {children}
        </Content>
      </section>
    </Layout>
  )
}

export default LayoutComponent