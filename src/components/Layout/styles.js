import styled from 'styled-components'

export const Layout = styled.section `
  display: grid;
  grid-template-columns: 126px auto;
  margin: 0 auto;
  padding: 0 0 21px;
  width: 100%;
`
export const Content = styled.section`
  padding: 0 50px;
  padding-top: 26px;
`