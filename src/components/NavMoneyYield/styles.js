import styled, { css } from 'styled-components'

import { TabItem, TabItemValue, AccountValue, TabItemText } from '../NavSummaryAccount/styles'

export const NavMoneyYield = styled.ul`
  display: flex;
  flex-direction: column;
  align-items: center;
  min-width: 204px;
  border-right: 1px solid #e3e3e3;
  box-sizing: border-box;
`

export const NavMoneyItem = styled(TabItem)`
  margin: 0;
  border-bottom: 1px solid #e3e3e3;
  padding: 20px 0 22px 0;
  width: 100%;
  justify-content: flex-start;
  padding-left: 20px;
  box-sizing: border-box;
  cursor: pointer;
  transition: all ease-in .1s;

  ${props => props.active===true && css`
    background-color: #4a90e2b3;
  `};

  &:first-child {
    padding: 26px 0 22px 0;
    &:after,
    &:before {
      content: none
    }
  }

  &:not(:first-child) {
    display: flex;
    align-items: center;
    flex-direction: row;
  }

  &:last-child {
    border: none
  }

  img {
    position: relative;
    left: 0;
  }

  img:first-child {
    margin-right: 19px;
  }

  img:last-child {
    position:absolute;
    left: 45%;
    bottom: -13px;
    z-index: 9;
  }
`

export const YieldValue = styled(AccountValue)`
  color: #3e3e3e;
`

export const YieldText = styled(TabItemText)`
  text-align: left;
`
export const YieldItemValue = styled(TabItemValue)`
  text-align: left;
  padding-bottom: 0;
`