import React from 'react'

import { Icon } from '..'
import { NavMoneyYield, NavMoneyItem, YieldValue, YieldText, YieldItemValue } from './styles'
import { AccountValueTitle } from '../NavSummaryAccount/styles'

const NavMoneyYieldComponent = props => {
  const { onClick, menuActived } = props

  return (
    <NavMoneyYield>
      <NavMoneyItem active={menuActived === 'myYield'} onClick={() => { onClick('myYield') }}>
        <AccountValueTitle>Mi Rendimiento:</AccountValueTitle>
        <YieldValue>$435, 000</YieldValue>
      </NavMoneyItem>
      <NavMoneyItem active={menuActived === 'capitalGain'} onClick={() => { onClick('capitalGain') }}>
        <Icon type='rentaAnualActual1' />
        <div>
          <YieldText>Plusvalia</YieldText>
          <YieldItemValue>$150,00</YieldItemValue>
        </div>
        <Icon type='plus' width={25} height={25} />
      </NavMoneyItem>
      <NavMoneyItem active={menuActived === 'utilitySales'} onClick={() => { onClick('utilitySales') }}>
        <Icon type='handshake' />
        <div>
          <YieldText>Utilidad de Ventas</YieldText>
          <YieldItemValue>$100,00</YieldItemValue>
        </div>
        <Icon type='plus' width={25} height={25} />
      </NavMoneyItem>
      <NavMoneyItem active={menuActived === 'currentRevenue'} onClick={() => { onClick('currentRevenue') }}>
        <Icon type='rentaAnualActual' />
        <div>
          <YieldText>Rentas recibidas</YieldText>
          <YieldItemValue>$180,00</YieldItemValue>
        </div>
        <Icon type='plus' width={25} height={25} />
      </NavMoneyItem>
      <NavMoneyItem active={menuActived === 'currentYearRevenue'} onClick={() => { onClick('currentYearRevenue') }}>
        <Icon type='rentaAnualActual1' />
        <div>
          <YieldText>Otros</YieldText>
          <YieldItemValue>$5,00</YieldItemValue>
        </div>
      </NavMoneyItem>
    </NavMoneyYield>
  )
}

export default NavMoneyYieldComponent