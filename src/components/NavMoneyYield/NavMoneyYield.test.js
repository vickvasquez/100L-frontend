import { screen, render } from '@testing-library/react'
import { axe } from 'jest-axe'

import NavMoneyYieldComponent from '.'

import NavMoneyYield from '.'

describe('NavMoneyYield component', () => {
  it('should be accesible', async () => {
    const { container } = render(<NavMoneyYield />)

    const result = await axe(container)
    expect(result).toHaveNoViolations()
  })
})