
import React from 'react'

import { Icon } from '..'
import { CardSummaryBricks, NavSummary, NavSummaryItem, SummaryItemValue, ContainerImage } from './styles'
import { TabItemText } from '../NavSummaryAccount/styles'

const CardSummaryBricksComponent = props => {
  const { bricksAvailable = 35, bricksInPortfolio = 5 } = props
  return (
    <CardSummaryBricks>
      <ContainerImage>
        <Icon type='ladrillosDisponibles' width={72} height={38} />
        <SummaryItemValue style={{ paddingTop: '9px', paddingBottom: '7px'}}>40 Ladrillos</SummaryItemValue>
      </ContainerImage>
      <NavSummary>
        <NavSummaryItem>
          <TabItemText>Ladrillos en portafolio</TabItemText>
          <SummaryItemValue>{bricksAvailable}</SummaryItemValue>
          <Icon type='plus' width={24} height={24} />
        </NavSummaryItem>
        <NavSummaryItem>
          <TabItemText>Ladrillos en Venta</TabItemText>
          <SummaryItemValue>{bricksInPortfolio}</SummaryItemValue>
        </NavSummaryItem>
      </NavSummary>
    </CardSummaryBricks>
  )
}

export default CardSummaryBricksComponent