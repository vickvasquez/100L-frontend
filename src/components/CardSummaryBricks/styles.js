import styled from 'styled-components'
import { TabItemValue } from '../NavSummaryAccount/styles'

export const CardSummaryBricks = styled.div`
  border-right: 3px solid #e3e3e3;
  padding: 29px 15px 0 15px;
`

export const ContainerImage = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
`

export const NavSummary = styled.ul`
  border-top: 1px solid #e3e3e3;
  display: flex;
`

export const NavSummaryItem = styled.li`
  list-style: none;
  padding: 14px 0 8px 0;
  position: relative;

 &:nth-child(1) {
    &:before,
    &:after {
      content: '';
      position: absolute;
      width: 2px;
      height: 35%;
      background-color: #e3e3e3;
      right: 0;
    }

    &:before {
      top: 0;
    }

    &:after {
      top: 80%;
    }

   img {
     position: absolute;
     top: 45%;
     right: -11.12px;
   }
 }
`

export const SummaryItemValue = styled(TabItemValue)`
  font-size: 24px;
`