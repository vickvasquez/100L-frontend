import React from 'react'

import { SpinnerWrapper, Placeholder } from './styles'

const SpinnerComponent = props => {
  const { message = 'Cargando datos...' } = props
  return (
    <SpinnerWrapper>
      <Placeholder>{message}</Placeholder>
    </SpinnerWrapper>
  )
}

export default SpinnerComponent