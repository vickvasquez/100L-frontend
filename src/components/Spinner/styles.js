import styled from 'styled-components'

export const SpinnerWrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  position: absolute;
  left: 0;
  top: 0;
  right: 0;
  bottom: 0;
`

export const Placeholder = styled.p`
  font-weight: 500;
  font-size: 24px;
`