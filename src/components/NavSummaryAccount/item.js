import React from 'react'

import { TabItem } from './styles'

const TabItemContent = props => {
  const { children } = props

  return (
    <TabItem role='presentation'>
      {children}
    </TabItem>
  )
}

export default TabItemContent