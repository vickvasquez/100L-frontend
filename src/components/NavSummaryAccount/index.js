import React from 'react'

import NavItem from './item'
import { Icon } from '..'
import  { NavSummaryAccountWrapper, AccountValueTitle, AccountValue, TabItemText, TabItemValue } from './styles'

const NavSummaryAccountComponent = props => {
  return (
    <NavSummaryAccountWrapper>
      <NavItem>
        <AccountValueTitle>Valor de tu cuenta</AccountValueTitle>
        <AccountValue>$1,000,000</AccountValue>
      </NavItem>
      <NavItem>
        <TabItemText>Inversión ladrillos</TabItemText>
        <TabItemValue>$700,000</TabItemValue>
        <Icon type='plus' />
      </NavItem>
      <NavItem>
        <TabItemText>Plusvalia</TabItemText>
        <TabItemValue>$150,000</TabItemValue>
        <Icon type='plus' />
      </NavItem>
      <NavItem>
        <TabItemText>Fondo Revolvente</TabItemText>
        <TabItemValue>$30,000</TabItemValue>
        <Icon type='plus' />
      </NavItem>
      <NavItem>
        <TabItemText>Ladrillos en proceso de compra</TabItemText>
        <TabItemValue underline>$25,000</TabItemValue>
        <Icon type='plus' />
      </NavItem>
      <NavItem>
        <TabItemText>Renta pendiente de liberar</TabItemText>
        <TabItemValue underline>$85,000</TabItemValue>
        <TabItemValue underline variant='danger'>Liberar</TabItemValue>
        <Icon type='plus' />
      </NavItem>
      <NavItem>
        <TabItemText>Dinero disponible</TabItemText>
        <TabItemValue variant='success'>$10,000</TabItemValue>
      </NavItem>
    </NavSummaryAccountWrapper>
  )
}

export default NavSummaryAccountComponent