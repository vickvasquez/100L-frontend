import styled, { css } from 'styled-components'

const variants = {
  danger: '#ff5a60',
  success: '#48b83b'
}

export const NavSummaryAccountWrapper = styled.ul`
  display: flex;
  flex-direction:  row;
  align-items: center;
  margin: 0;
  padding: 0;
  border-radius: 8px;
  box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.2), 0 2px 2px 0 rgba(0, 0, 0, 0.12), 0 0 2px 0 rgba(0, 0, 0, 0.14);
  box-sizing: border-box;
  width: 100%;
  height: 90px;
  overflow: auto hidden;
  justify-content: space-between;
`

export const TabItem = styled.li`
  list-style-type: none;
  display: flex;
  justify-content: center;
  flex-direction: column;
  padding: 17px 30px 12px 21px;
  position: relative;
  margin-left: 14.8px;

  img {
    position: absolute;
    right: -32px;
  }

  &:first-child {
    background-color: #fff;
    margin-left: 0;

    &:after,
    &:before {
      content: '';
      position: absolute;
      top: 0;
      right: -22.4px;
      bottom: 0;
    }

    &:after {
      border-top: 40px solid transparent;
      border-bottom: 40px solid transparent;
      border-left: 22.4px solid white;
    }
    &:before {
      border-top: 40.5px solid transparent;
      border-bottom: 40.5px solid transparent;
      border-left: 22.4px solid rgba(0, 0, 0, 0.2);
      right: -23.5px;
    }
  }
`
const NavItemText = styled.p`
  text-align: center;
  padding-bottom: 5px;
`

export const AccountValue = styled(NavItemText)`
  font-size: 25px;
  font-weight: bold;
  color: #0477c0;
`

export const AccountValueTitle = styled(NavItemText)`
  font-size: 24px;
`
export const TabItemText = styled(NavItemText)`
  font-size: 14px;
  letter-spacing: normal;
`

export const TabItemValue = styled(NavItemText)`
  font-size: 18px;
  font-weight: bold;
  color: ${props => variants[props.variant] || 'black'};
  ${props => props.underline && css`
    text-decoration: underline
  `}
`