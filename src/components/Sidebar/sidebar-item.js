import React from 'react'

import { Icon } from '..'
import RenderIf from '../../RenderIf'

import { SidebarItem, Label, Link, LabelMoney } from './styles'

const SidebarItemContent = props => {
  const { icon, label, value, children } = props
  return (
    <React.Fragment>
      <RenderIf condition={icon}>
        <Icon type={icon} />
      </RenderIf>
      <RenderIf condition={label}>
        <Label> {label} </Label>
      </RenderIf>
      <RenderIf condition={value}>
        <Label>$ <LabelMoney>{value}</LabelMoney></Label>
      </RenderIf>
      {children}
    </React.Fragment>
  )
}
const SidebarItemComponent = props => {
  const { as, bordered, onClick= () => {}, active, label } = props

  const actived = active === label

  if (as !== 'a') {
    return <SidebarItem onClick={() => { onClick(label)}} active={actived} bordered={bordered}>
        <SidebarItemContent {...props} />
      </SidebarItem>
  }

  return <SidebarItem onClick={() => { onClick(label)}} active={actived} bordered={bordered}>
    <Link href='/'>
      <SidebarItemContent {...props} />
    </Link>
  </SidebarItem>
}

export default SidebarItemComponent