import React, { useState } from 'react'

import items from '../../data/sidebar'
import summarySaldo from '../../data/summarySaldo'

import { Icon } from '..'
import SidebarItem from './sidebar-item'

import { Sidebar, ContainerItems } from './styles'

const SidebarComponent = () => {
  const [menuActived, setMenuActived] = useState('Mi cuenta')

  const handleClick = name => {
    setMenuActived(name)
  }

  return (
    <Sidebar>
      <ContainerItems>
        <SidebarItem active={false} as='a' bordered>
          <Icon type='logotipo' width={90} height={40} />
        </SidebarItem>

        {summarySaldo.map(item => (
          <SidebarItem bordered label={item.label} value={item.value} key={item.value} />
        ))}

      </ContainerItems>
      <ContainerItems>
        {items.map(item => (
          <SidebarItem active={menuActived} onClick={handleClick} icon={item.icon} label={item.label} key={item.label || item.icon} />
        ))}
      </ContainerItems>
    </Sidebar>
  )
}

export default SidebarComponent