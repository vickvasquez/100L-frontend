import { render, screen } from '@testing-library/react'
import { axe } from 'jest-axe'

import SidebarItem from './sidebar-item'
import { ContainerItems } from './styles'

describe('SidebarItem component', () => {
  it('should be accesible', async () => {
    const { container } = render(<ContainerItems>
        <SidebarItem label='Comprar' icon='comprar' />
      </ContainerItems>
    )

    const result = await axe(container)
    expect(result).toHaveNoViolations()
  })

  it('should display an sidebarItem', () => {
    render(<SidebarItem label='Comprar' icon='comprar' />)

    const sidebarItem = screen.getByRole('listitem')
    expect(sidebarItem).toBeInTheDocument()
  })

  describe('should display an sidebarItem with', () => {
    it('icon', () => {
      render(<SidebarItem label='Comprar' icon='comprar' />)
      const icon = screen.getByRole('img')
      expect(icon).toBeInTheDocument()

    })

    it('text', () => {
      render(<SidebarItem label='Comprar' icon='comprar' />)
      const text = screen.getByText(/Comprar/i)
      expect(text).toBeInTheDocument()
    })

  })
})
