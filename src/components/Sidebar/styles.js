import styled, { css } from 'styled-components'

export const Sidebar = styled.nav`
  background-color: #fff;
  min-height: 100%;
`

export const ContainerItems = styled.ul`
  margin: 0;
  padding: 0;
  display: flex;
  flex-direction: column;
`

export const SidebarItem = styled.li`
  margin: 0;
  padding-top: 14px;
  padding-bottom: 10px;
  list-style-type: none;
  cursor: pointer;
  display: flex;
  flex-direction: column;
  align-items: center;
  transition: all .2s ease-in;

  ${props => props.active === true && css`
    background-color: #dddddd;
    box-shadow: inset 0 1px 7px 0 rgba(134, 134, 134, 0.75);
    opacity: 0.48;
    color: #f53923;
    border-left: 4px solid #f53923;
    opacity: 1;
  `}

  ${props => props.bordered && css`
    border-bottom: solid 1px #e1e1e1;
  `}
`
export const Label = styled.small`
  text-align: center;
  font-size: 13px;
  margin-top: 5px;
  display: block;
`
export const LabelMoney = styled(Label)`
  display: inline-block;
  font-size: 16px;
`

export const Link = styled.a`
  text-decoration: none;
  text-align: center;
  color: #3e3e3e;
`