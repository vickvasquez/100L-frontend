import { render, screen } from '@testing-library/react'
import { axe } from 'jest-axe'

import Sidebar from '.'

import items from '../../data/sidebar'

const itemsSidebar = items.map(item => item.label)
describe('Sidebar component', () => {
  it('should be accesible', async () => {
    const { container } = render(
      <Sidebar />
    )

    const result = await axe(container)

    expect(result).toHaveNoViolations()
  })

  it('should have logotipo', () => {
    render(
      <Sidebar />
    )

    const logotipo = screen.getByAltText('logotipo.svg')
    expect(logotipo).toBeInTheDocument()
  })

  test.each(itemsSidebar)('check if Navbar have %s', (item) => {
    render(
      <Sidebar />
    )

    if (item) {
      const sidebarItem = screen.getByText(new RegExp(item), 'i')
      expect(sidebarItem).toBeInTheDocument()
    }
  })
})