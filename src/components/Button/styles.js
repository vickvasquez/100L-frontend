import styled from 'styled-components'

export const Button = styled.button`
  background-color: #ff5a60;
  color: #f8f8f8;
  padding: 11px 20px;
  border-radius: 24px;
  text-align: center;
  font-weight: 700;
  border: 0;
  outline: 0;
  cursor: pointer;
  transition: all ease-in .3s;
  border: 1px solid #ff5a60;

  &:hover {
    color: #ff5a60;
    background-color: #f8f8f8;
    border: 1px solid  #ff5a60;
  }
`