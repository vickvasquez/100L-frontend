import { screen, render } from '@testing-library/react'
import { axe } from 'jest-axe'
import userEvent from '@testing-library/user-event'

import Button from '.'

describe('Button component', () => {
  it('should be accesible', async () => {
    const { container } = render(<Button>My button</Button>)
    const result = await axe(container)
    expect(result).toHaveNoViolations()
  })

  it('should render the button', () => {
    render(<Button>My button</Button>)

    const button = screen.getByRole('button')
    expect(button).toBeInTheDocument()
  })

  it('should render button with text', () => {
    render(<Button>My button</Button>)
    const button = screen.getByText('My button')

    expect(button).toBeInTheDocument()
  })

  it('should action on click', () => {
    const handleClick = jest.fn()
    render(<Button onClick={handleClick}>My button</Button>)

    const button = screen.getByRole('button')
    userEvent.click(button)

    expect(handleClick).toHaveBeenCalledTimes(1)
  })
})