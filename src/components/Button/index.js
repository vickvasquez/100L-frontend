import React from 'react'

import { Button } from './styles'

const ButtonComponent = props => {
  const { children, onClick } = props

  return (
    <Button onClick={onClick}>{children}</Button>
  )
}

export default ButtonComponent