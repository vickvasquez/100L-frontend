## Challenge Frontend 100L

1.- Clone repo

2.- Install packages `npm i`

3.- Create `.env.local` file with content
```
REACT_APP_API_ENDPOINT=https://api.jsonbin.io
REACT_APP_SECRET_KEY=
```

4.- Start app `npm start`

5.- Run test `npm test`
